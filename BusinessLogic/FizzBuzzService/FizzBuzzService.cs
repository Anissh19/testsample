﻿using BusinessLogic.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogic.FizzBuzzServices
{
    public class FizzBuzzService : IFizzBuzzService
    {
        private IEnumerable<IDisplayMessageStrategy> FizzBuzzCondition;

        public FizzBuzzService(IEnumerable<IDisplayMessageStrategy> strategies)
        {
            this.FizzBuzzCondition = strategies;
        }

        public IEnumerable<string> GetList(int number)
        {
            var outputList = new List<string>();
            for (int index = 1; index <= number; index++)
            {
                var isDivisible = this.FizzBuzzCondition.Where(m => m.IsMatch(index)).ToList();
                outputList.Add(isDivisible.Any() ? string.Join(" ", isDivisible.Select(m => m.GetMessages())) : index.ToString());
            }
            return outputList;
        }
    }
}
