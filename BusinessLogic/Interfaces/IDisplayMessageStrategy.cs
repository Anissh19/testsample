﻿namespace BusinessLogic.Interfaces
{
    public interface IDisplayMessageStrategy
    {
        bool IsMatch(int number);

        string GetMessages();
    }
}
