﻿using System.Collections.Generic;

namespace BusinessLogic.Interfaces
{
    public interface IFizzBuzzService
    {
        IEnumerable<string> GetList(int number);
    }
}
