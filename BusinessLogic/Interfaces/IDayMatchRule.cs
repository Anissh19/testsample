﻿namespace BusinessLogic.Interfaces
{
    public interface IDayMatchRule
    {
        bool IsDayMatch();
    }
}
