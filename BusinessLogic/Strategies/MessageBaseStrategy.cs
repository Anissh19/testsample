﻿using BusinessLogic.Interfaces;

namespace BusinessLogic.Strategies
{
    public class MessageBaseStrategy : IDisplayMessageStrategy
    {
        protected int DivisibleBy { get; set; }

        protected string DefaultMessage { get; set; }

        private IDayMatchRule dayMatch;
        public string ExceptionalDayMessage { get; }

        public MessageBaseStrategy(IDayMatchRule dayMatch, int DivisibleBy, string DefaultMessage, string ExceptionalDayMessage)
        {
            this.ExceptionalDayMessage = ExceptionalDayMessage;
            this.dayMatch = dayMatch;
            this.DivisibleBy = DivisibleBy;
            this.DefaultMessage = DefaultMessage;
        }

        public bool IsMatch(int number)
        {
            return number % this.DivisibleBy == 0;
        }

        public string GetMessages()
        {
            return dayMatch.IsDayMatch() ? this.ExceptionalDayMessage : this.DefaultMessage;
        }
    }
}
