﻿using BusinessLogic.Interfaces;

namespace BusinessLogic.Strategies
{
    public class DivisibleByFiveStrategy : MessageBaseStrategy
    {
        public DivisibleByFiveStrategy(IDayMatchRule dayMatch) : base(dayMatch, 5, "Buzz", "Wuzz")
        {
        }
    }
}
