﻿using BusinessLogic.Interfaces;
using System;

namespace BusinessLogic.Strategies
{
    public class DayMatchRule : IDayMatchRule
    {
        private DayOfWeek exceptionalDay = DayOfWeek.Wednesday;
        private DayOfWeek currentDay = DateTime.Now.DayOfWeek;

        public bool IsDayMatch()
        {
            return exceptionalDay == currentDay;
        }
    }
}
