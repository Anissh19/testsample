﻿using BusinessLogic.Interfaces;

namespace BusinessLogic.Strategies
{
    public class DivisibleByThreeStrategy : MessageBaseStrategy
    {
        public DivisibleByThreeStrategy(IDayMatchRule dayMatch) : base(dayMatch, 3, "Fizz", "Wizz")
        {

        }
    }
}
