﻿using BusinessLogic.FizzBuzzServices;
using BusinessLogic.Interfaces;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogic.Tests
{
    [TestFixture]
    public class FizzBuzzServiceTests
    {
        Mock<IDisplayMessageStrategy> mockFizz;
        Mock<IDisplayMessageStrategy> mockBuzz;
        IEnumerable<IDisplayMessageStrategy> strategies;
        IFizzBuzzService fizzBuzzService;

        [SetUp]
        public void Setup_FizzBuzzServiceTest()
        {
            mockFizz = new Mock<IDisplayMessageStrategy>();
            mockBuzz = new Mock<IDisplayMessageStrategy>();
            strategies = new List<IDisplayMessageStrategy>() { mockFizz.Object, mockBuzz.Object };
            fizzBuzzService = new FizzBuzzService(strategies);
        }

        [Test]
        public void GetListReturnsListsWithFizzBuzz_When_NumberIsValid()
        {
            mockFizz.Setup(s => s.IsMatch(3)).Returns(true);
            mockFizz.Setup(s => s.GetMessages()).Returns("Fizz");
            mockBuzz.Setup(s => s.IsMatch(5)).Returns(true);
            mockBuzz.Setup(s => s.GetMessages()).Returns("Buzz");

            var testList = (fizzBuzzService.GetList(15));

            Assert.AreEqual(testList.ElementAt(0), "1");
            Assert.AreEqual(testList.ElementAt(1), "2");
            Assert.AreEqual(testList.ElementAt(2), "Fizz");
            Assert.AreEqual(testList.ElementAt(3), "4");
            Assert.AreEqual(testList.ElementAt(4), "Buzz");
        }

        [TearDown]
        public void TearDown_FizzBuzzServiceTest()
        {
            mockFizz = null;
            mockBuzz = null;
            strategies = null;
            fizzBuzzService = null;
        }
    }
}
