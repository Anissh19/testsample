﻿using BusinessLogic.Strategies;
using NUnit.Framework;
using System;

namespace BusinessLogic.Tests
{
    [TestFixture]
    public class DayMatchTests
    {
        [Test]
        public void IsDayMatchReturnsAppropriateValue_ForExceptionalDayAndNormalDay()
        {
            var dayMatch = new DayMatchRule();
            bool expected = DateTime.Now.DayOfWeek == DayOfWeek.Thursday;

            bool result = dayMatch.IsDayMatch();

            Assert.AreEqual(result, expected);
        }
    }
}
