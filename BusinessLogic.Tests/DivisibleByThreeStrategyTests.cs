﻿using BusinessLogic.Interfaces;
using BusinessLogic.Strategies;
using Moq;
using NUnit.Framework;

namespace BusinessLogic.Tests
{
    [TestFixture]
    public class DivisibleByThreeStrategyTests
    {
        private Mock<IDayMatchRule> mockDay;
        DivisibleByThreeStrategy Strategy;

        [SetUp]
        public void Setup_DivisibleByThreeStrategyTests()
        {
            mockDay = new Mock<IDayMatchRule>();
            Strategy = new DivisibleByThreeStrategy(mockDay.Object);
        }

        [Test]
        public void IsMatchReturnsTrue_WhenInputNumberIsDivisibleByThree()
        {
            var result = Strategy.IsMatch(9);

            Assert.IsTrue(result);
        }

        [Test]
        public void IsMatchReturnsFalse_WhenInputNumberIsNotDivisibleByThree()
        {
            var result = Strategy.IsMatch(8);

            Assert.IsFalse(result);
        }

        [Test]
        public void GetMessageReturnsFizz_When_NumberIsDivisibleByThreeAnd_DayIsNotExceptionalDay()
        {
            var result = Strategy.GetMessages();

            Assert.AreEqual("Fizz", result);
        }

        [Test]
        public void GetMessagesReturnWizz_When_NumberIsDivisibleByThree_And_DayIsExceptionalDay()
        {

            mockDay.Setup(s => s.IsDayMatch()).Returns(true);

            var result = Strategy.GetMessages();

            Assert.AreEqual("Wizz", result);
        }

        [TearDown]
        public void TearDown_DivisibleByThreeTests()
        {
            mockDay = null;
        }
    }
}
