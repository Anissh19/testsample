﻿using BusinessLogic.Interfaces;
using BusinessLogic.Strategies;
using Moq;
using NUnit.Framework;

namespace BusinessLogic.Tests
{
    [TestFixture]
    public class DivisibleByFiveStrategyTests
    {
        private Mock<IDayMatchRule> mockDay;
        DivisibleByFiveStrategy Strategy;

        [SetUp]
        public void Setup_DivisibleByFiveStrategyTest()
        {
            mockDay = new Mock<IDayMatchRule>();
            Strategy = new DivisibleByFiveStrategy(mockDay.Object);
        }

        [Test]
        public void IsMatchReturnsTrue_WhenInputNumberIsDivisibleByFive()
        {
            var result = Strategy.IsMatch(10);

            Assert.IsTrue(result);

        }

        [Test]
        public void IsMatchReturnsFalse_WhenInputNumberIsNotDivisibleByFive()
        {
            var result = Strategy.IsMatch(8);

            Assert.IsFalse(result);

        }

        [Test]
        public void GetMessagesReturnBuzz_When_NumberIsDivisibleByFive_And_DayIsNotExceptionalDay()
        {
            var result = Strategy.GetMessages();

            Assert.AreEqual("Buzz", result);
        }

        [Test]
        public void GetMessagesReturnWuzz_When_NumberIsDivisibleByFive_And_DayIsExceptionalDay()
        {
            mockDay.Setup(s => s.IsDayMatch()).Returns(true);

            var result = Strategy.GetMessages();

            Assert.AreEqual("Wuzz", result);
        }

        [TearDown]
        public void TearDown_DivisibleByFiveTests()
        {
            mockDay = null;
        }
    }
}
