﻿using BusinessLogic.Interfaces;
using FizzBuzz.Web.Models;
using Microsoft.AspNetCore.Mvc;
using X.PagedList;

namespace FizzBuzz.Web.Controllers
{
    public class HomeController : Controller
    {
        private IFizzBuzzService FizzBuzzService;
        private const int PageSize = 20;

        public HomeController(IFizzBuzzService fizzbuzzService)
        {
            this.FizzBuzzService = fizzbuzzService;
        }

        public IActionResult Index()
        {
            return View("Index");
        }

        public IActionResult GetFizzBuzzList(FizzBuzzModel fizzbuzzModel, int? page)
        {
            if (ModelState.IsValid)
            {
                int pageNumber = page.HasValue ? page.Value : 1;
                var fizzBuzzList = FizzBuzzService.GetList(fizzbuzzModel.InputNumber);
                fizzbuzzModel.FizzBuzzList = fizzBuzzList.ToPagedList(pageNumber, PageSize);
                return View("Result", fizzbuzzModel);
            }
            return View("Index", fizzbuzzModel);
        }
    }
}
