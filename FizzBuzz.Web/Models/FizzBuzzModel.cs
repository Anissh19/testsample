﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using X.PagedList;

namespace FizzBuzz.Web.Models
{
    public class FizzBuzzModel
    {
        [Range(1, 1000, ErrorMessage = "Please enter a valid number from 1 to 1000")]
        [Required(ErrorMessage = "Please enter number")]
        [DisplayName("Enter the number")]
        public int InputNumber { get; set; }

        public IPagedList<string> FizzBuzzList { get; set; }
    }
}
