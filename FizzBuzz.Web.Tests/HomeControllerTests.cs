﻿using BusinessLogic.Interfaces;
using FizzBuzz.Web.Controllers;
using FizzBuzz.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace FizzBuzz.Web.Tests
{
    [TestFixture]
    public class HomeControllerTests
    {
        private Mock<IFizzBuzzService> mockFizzBuzz;
        private HomeController fizzBuzzController;
        private List<string> fizzBuzzList;
        int page = 1;

        [SetUp]
        public void SetUp_test()
        {
            mockFizzBuzz = new Mock<IFizzBuzzService>();
            fizzBuzzController = new HomeController(mockFizzBuzz.Object);
            fizzBuzzList = new List<string>()
            {
                "1","2","Fizz","4","Buzz"
            };
        }

        [Test]
        public void IndexActionReturns_IndexPage_AsViewResult()
        {
            var actual = fizzBuzzController.Index() as ViewResult;

            Assert.AreEqual(actual.ViewName, "Index");
        }

        [Test]
        public void GetFizzBuzzActionReturnsViewWithFizzBuzzList_WhenValidNumberIsEntered()
        {
            var fizzBuzzService = new Mock<IFizzBuzzService>();
            var controllerObject = new HomeController(fizzBuzzService.Object);
            var viewModel = new FizzBuzzModel
            {
                InputNumber = 6
            };
            ViewResult result = controllerObject.GetFizzBuzzList(viewModel, page) as ViewResult;

            fizzBuzzService.Setup(x => x.GetList(6)).Returns(fizzBuzzList);

            Assert.AreEqual(result.ViewName, "Result");
            Assert.IsInstanceOf(typeof(FizzBuzzModel), result.Model);
        }
    }
}
